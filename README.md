SAP Report Source Decompressor
==============================
[![GitLab CI](https://gitlab.com/daberlin/sap-reposrc-decompressor/badges/master/pipeline.svg)](https://gitlab.com/daberlin/sap-reposrc-decompressor/commits/master)
[![AppVeyor](https://ci.appveyor.com/api/projects/status/gitlab/daberlin/sap-reposrc-decompressor?svg=true)](https://ci.appveyor.com/project/daberlin/sap-reposrc-decompressor)

&rArr; http://www.daniel-berlin.de/devel/sap-dev/decompress-abap-source-code/
